<?php 
/*
 * A Design by W3layouts
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
 *
 */
include "app/config.php";

if ($page_name=='') {
	include 'web/index.html';
	}
elseif ($page_name=='index.html') {
	include 'web/index.html';
	}
elseif ($page_name=='index.ca.html') {
	include 'web/index.ca.html';
	}
elseif ($page_name=='index.en.html') {
	include 'web/index.en.html';
	}
elseif ($page_name=='home.html') {
	include 'web/home.html';
	}
elseif ($page_name=='about.html') {
	include 'web/about.html';
	}
elseif ($page_name=='about.ca.html') {
	include 'web/about.ca.html';
	}
elseif ($page_name=='about.en.html') {
	include 'web/about.en.html';
	}
elseif ($page_name=='edificacion.html') {
	include 'web/edificacion.html';
	}
elseif ($page_name=='edificacion.ca.html') {
	include 'web/edificacion.ca.html';
	}
elseif ($page_name=='edificacion.en.html') {
	include 'web/edificacion.en.html';
	}
elseif ($page_name=='unifamiliares.html') {
	include 'web/unifamiliares.html';
}
elseif ($page_name=='unifamiliares.ca.html') {
	include 'web/unifamiliares.ca.html';
}
elseif ($page_name=='unifamiliares.en.html') {
	include 'web/unifamiliares.en.html';
}
elseif ($page_name=='plurifamiliares.html') {
	include 'web/plurifamiliares.html';
}
elseif ($page_name=='plurifamiliares.ca.html') {
	include 'web/plurifamiliares.ca.html';
}
elseif ($page_name=='plurifamiliares.en.html') {
	include 'web/plurifamiliares.en.html';
}
elseif ($page_name=='rehabilitacion.html') {
	include 'web/rehabilitacion.html';
}
elseif ($page_name=='rehabilitacion.ca.html') {
	include 'web/rehabilitacion.ca.html';
}
elseif ($page_name=='rehabilitacion.en.html') {
	include 'web/rehabilitacion.en.html';
}
elseif ($page_name=='urbanismo.html') {
	include 'web/urbanismo.html';
}
elseif ($page_name=='urbanismo.ca.html') {
	include 'web/urbanismo.ca.html';
}
elseif ($page_name=='urbanismo.en.html') {
	include 'web/urbanismo.en.html';
}
elseif ($page_name=='otros.html') {
	include 'web/otros.html';
}
elseif ($page_name=='otros.ca.html') {
	include 'web/otros.ca.html';
}
elseif ($page_name=='otros.en.html') {
	include 'web/otros.en.html';
}
elseif ($page_name=='contact.html') {
	include 'web/contact.html';
	}
elseif ($page_name=='contact.ca.html') {
	include 'web/contact.ca.html';
	}
elseif ($page_name=='contact.en.html') {
	include 'web/contact.en.html';
	}
elseif ($page_name=='404.html') {
	include 'web/404.html';
	}
elseif ($page_name=='404.ca.html') {
	include 'web/404.ca.html';
	}
elseif ($page_name=='404.en.html') {
	include 'web/404.en.html';
	}
elseif ($page_name=='contact-post.html') {
	include 'app/contact.php';
	}
else
	{
		include 'web/404.html';
	}

?>
